package com.carlosandres.epamcourse;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 * Unit test for simple App.
 */
//@RunWith(Parameterized.class)
public class AppTest {

	public AppTest() {
		super();
	}

	private List<String> numbers;

//	public AppTest(String... values) {
//		numbers = new LinkedList<String>();
//		for (int i = 0; i < values.length; i++) {
//			numbers.add(values[i]);
//		}
//	}

	App app = new App();

	@Test
	public void testAppWithNoArguments() {
		String result = app.sortValues();
		assertEquals("", result);
	}

	@Test
	public void testAppWithOneArgument() {
		String[] param = { "5" };
		String result = app.sortValues(param);
//		String result = app.sortValues(numbers.get(0));
		assertEquals("5", result);
	}

	@Test
	public void testAppWithMultipleArguments() {
		String[] param = { "10", "5", "8", "3", "1" };
		String result = app.sortValues(param);
//		String result = app.sortValues(numbers.toArray(new String[0]));
		assertEquals("1 3 5 8 10", result);
	}

	@Test(expected = IllegalArgumentException.class)
	public void testAppWithInvalidArgument() {
		String[] param = { "10", "5", "8", "invalid", "1" };
		String result = app.sortValues(param);
//		String result = app.sortValues(numbers.toArray(new String[0]));
	}

//	@Parameterized.Parameters
	public static Collection<Object[]> data() {
		return Arrays.asList(new Object[][] { //
				{ "", "", "" }, // No real roots
				{ "", "", "" }, // No real roots
				{ "", "", "" }, // No real roots
				{ "", "", "" } // No real roots
		});
	}

}
