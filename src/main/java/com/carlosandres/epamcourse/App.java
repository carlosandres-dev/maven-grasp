package com.carlosandres.epamcourse;

import java.util.Arrays;

/**
 * This is my personal evidence that shows the level of understanding and use of
 * Maven
 * 
 * @author Carlos Andres
 * @version 1.0
 * @since 1.0
 */
public class App {

	public App() {
		super();
	}

	public static void main(String[] args) {
		App app = new App();
		String result = app.sortValues(args);
		System.out.println(result);
	}

	public String sortValues(String... values) {
		if (values == null || values.length == 0) {
			return "";
		}

		int[] intValues = new int[values.length];
		for (int i = 0; i < values.length; i++) {
			try {
				intValues[i] = Integer.parseInt(values[i]);
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException();
			} 
		}

		Arrays.sort(intValues);

		StringBuilder sortedValues = new StringBuilder();
		for (int i = 0; i < intValues.length; i++) {
			sortedValues.append(intValues[i]);
			if (i < intValues.length - 1) {
				sortedValues.append(" ");
			}
		}

		return sortedValues.toString();
	}

}
